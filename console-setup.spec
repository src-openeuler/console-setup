Name:           console-setup
Version:        1.230
Release:        1
Summary:        Console font and keymap setup program
License:        GPL-2.0-or-later AND MIT AND Public Domain
URL:            https://packages.debian.org/cs/sid/console-setup
Source0:        http://ftp.de.debian.org/debian/pool/main/c/%{name}/%{name}_%{version}.tar.xz
Patch0:         console-setup-1.76-paths.patch
Patch1:         console-setup-1.76-fsf-address.patch
Patch2:         console-setup-1.84-ctrll-lock.patch

BuildArch:      noarch
BuildRequires:  perl-generators perl(encoding)
Requires:       kbd

%description
This package provides the console with the same keyboard configuration scheme as the X Window System.
As a result, there is no need to duplicate or change the keyboard files just to make simple customizations
such as the use of dead keys, the key functioning as AltGr or Compose key, the key(s) to switch between
Latin and non-Latin mode, etc.

The package also installs console fonts supporting many of the world's languages. It provides an unified
set of font faces - the classic VGA, the simplistic Fixed, and the cleaned Terminus, TerminusBold and
TerminusBoldVGA.

%package_help

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1 -b .paths
%patch1 -p1 -b .fsf-address
%patch2 -p1 -b .ctrll-lock1

%build
make build-linux

%install
make prefix=%{buildroot} install-linux

%files
%doc README CHANGES
%license COPYRIGHT copyright.fonts copyright.xkb Fonts/copyright
%exclude %{_sysconfdir}/%{name}
%{_bindir}/*
%config(noreplace) %{_sysconfdir}/default/*
%{_datadir}/console*

%files help
%{_mandir}/man*/*

%changelog
* Wed Jul 31 2024 wangkai <13474090681@163.com> - 1.230-1
- Update to 1.230

* Mon Apr 17 2023 yaoxin <yao_xin001@hoperun.com> - 1.218-1
- Update to 1.218

* Tue Jan 18 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.207-1
- Upgrade to version 1.207

* Sun Sep 27 2020 yanan <yanan@huawei.com> - 1.184-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Remove Caps Lock to CtrlL_Lock substitution

* Mon Dec 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.184-5
- Package init
